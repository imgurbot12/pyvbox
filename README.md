vbox
===

A slightly more functional and easy to use wrapper
around the existing [virtualbox-api](https://github.com/SethMichaelLarson/virtualbox-python)
for python which allows greater control over vms and ensures proper
object cleanup.

### Components:
+ callback - extended callback utilities which allow for better control and management
+ misc     - miscellaneous utilities including a wrapped atexit manager which
             allows for proper cleanup included in all objects

### Notice:
this library is a WIP meaning I intend to continually add more functionality
and wrappers to this library as the need arises. Backwards compatibility
will **NOT** be maintained.
 